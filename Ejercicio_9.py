# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-


def matriz_identidad(matriz, ancho):

    for i in range(ancho):
        matriz.append([])
        for j in range(ancho):
            # La matriz identidad es aquella que en la dioganal posee
            # números 1; esto ocurre cuando las posiciones de j e i
            # son iguales.
            if(i == j):
                matriz[i].append("1 ")

            # De lo contrario, solo se rellena con 0.
            else:
                matriz[i].append("0 ")
            print(matriz[i][j], end="")
        print()

if __name__ == '__main__':

    matriz = []

    # Pide si o sí que se ingrese un número entero positivo; de lo
    # contrario, el programa no iniciará.
    while True:
        try:
            ancho = int(input("Ingrese el ancho de la matriz: "))

            if ancho > 0:
                matriz_identidad(matriz, ancho)
                break

            else:
                print("Debe ingresar un número entero positivo")

        except:
            print("Debe ingresar un valor válido (Solo enteros positivos)")
