# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-

import random

def rellena_lista(lista, largo):

    # Se rellena una lista con números random del 1 al 100.
    for i in range(1, largo + 1):
        lista.append(random.randrange(100))

    print("Su lista original es: ", lista)


def ordena_lista(lista, largo):

    for i in range(1, largo):
        for j in range(largo - i):
            # Cambio que busca dejar al final de la lista al número
            # que sea mayor.
            if(lista[j] > lista[j + 1]):

                # Serie de cambios que se realizan para cambiar
                # las posiciones de dos números; es necesario el temporal
                # ya que es el molde que se ocupará
                temporal = lista[j]
                lista[j] = lista[j + 1]
                lista[j + 1] = temporal

    print("\nSu lista ordenada es: ", lista)


if __name__ == '__main__':

    # Pide si o sí que se ingresen dos números enteros positivos; de lo
    # contrario, el programa no iniciará.
    while True:
        try:
            largo = int(input("Ingrese el largo de la lista: "))

            if (largo > 0):

                lista = []
                rellena_lista(lista, largo)
                ordena_lista(lista, largo)
                break

            else:
                print("Debe ingresar un número entero positivo")

        except:
            print("Por favor, ingrese un valor válido (Entero positivo)")
