# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-


def union_listas(a, b):

    # Lista nueva que contiene a las listas creadas con anterioridad.
    suma_listas = a + b

    print("La unión de las dos listas es: ", suma_listas)


def agrega_palabras(lista):

    # Utilizado para que se repita cada vez que la opcion ingresada
    # es una "s o S" y así poder seguir agregando palabras a la lista.
    while True:
        try:
            palabra = input("Ingrese la palabra que desea agregar a la lista: ")
            lista.append(palabra)

            opcion = input("¿Desea ingresar otra palabra? (S/N): ")

            if(opcion.upper() == 'S'):
                pass

            elif(opcion.upper() == 'N'):
                break

        except:
            print("\nDebe ingresar una palabra")


if __name__ == '__main__':

    lista_1 = []
    lista_2 = []

    agrega_palabras(lista_1)
    agrega_palabras(lista_2)

    print("La lista 1 es: ", lista_1)
    print("La lista 2 es: ", lista_2)

    union_listas(lista_1, lista_2)
