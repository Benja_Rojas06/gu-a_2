# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-


def particionador(string):

    # Se particiona el mensaje original para no modificarlo
    # y así poder usarlo a futuro; además se particiona cada dos espacios
    # y en reversa con tal de descriptar el mensaje.
    mensaje = string[::-2]
    print("El mensaje descriptado es: ", mensaje)


if __name__ == '__main__':

    string = "¡XeXgXaXsXsXeXmX XtXeXrXcXeXsX XaX XsXiX XsXiXhXt"
    particionador(string)
