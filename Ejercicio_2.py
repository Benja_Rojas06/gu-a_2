# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-


def dibujar_triangulo(altura_triangulo):

    # Se dibujan 2 triángulos con "*", uno al lado del otro
    # pero se generan de manera inversa
    for i in range(altura_triangulo + 1):
        # Espacios necesarios entre el primer triángulo y el
        # segundo para que se "forme" un triángulo entre estos
        espacios = (2*altura_triangulo) - (2*i)
        print('*' * i, ' ' * espacios + '*' * i)


if __name__ == '__main__':

    # Pide si o sí que se ingrese un número entero positivo; de lo
    # contrario, el programa no iniciará.
    while True:
        try:
            altura = int(input("Ingrese la altura del triángulo: ", ))

            if (altura > 0):

                altura_triangulo = altura + 1
                dibujar_triangulo(altura_triangulo)
                break

            else:
                print("Debe ingresar un número entero positivo")

        except:
            print("Debe ingresar un valor válido (Solo enteros positivos)")
