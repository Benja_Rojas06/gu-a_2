# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-


def invertir_lista(lista):

    # Inversión de la lista gracias a que se lee a la inversa.
    lista_invertida = lista[::-1]
    print("La lista invertida es: ", lista_invertida)


if __name__ == '__main__':

    # Lista creada para que sea invertida
    lista = ['Vendetta', 'por', 'mi', 'papa']

    invertir_lista(lista)
