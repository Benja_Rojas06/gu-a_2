# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-


import random


def edicion(n):

    # Creación de un duplicado para no afectar a la lista.
    lista_m = n[::]

    # Lista que solo contiene los múltiplos de 2 (De la
    # lista original) y dejando fuera a aquellos que contengan
    # el 0.
    lista_e = [n for n in lista_m if n % 2 == 0 and n % 10 != 0]

    print(lista_e)

def union_listas(a, b, c, largo):

    # Ordena la lista para que sea más fácil de comparar los
    # números que sean iguales.
    lista_n = a + b + c
    lista_n.sort()

    for i in range(1, largo):
        for j in range(1, largo):
            if(lista_n[i] == lista_n[i + 1]):
                del lista_n[i]

    print("Lista n: ", lista_n)
    edicion(lista_n)

def rellena_lista(lista, largo):

    # Rellena a las listas con números random del 1 al 100.
    for i in range(1, largo + 1):
        lista.append(random.randrange(100))


if __name__ == '__main__':

    lista_a = []
    lista_b = []
    lista_c = []

    # Pide si o sí que se ingrese un número entero positivo; de lo
    # contrario, el programa no iniciará.
    while True:

        try:
            largo = int(input("Ingrese el largo de la lista: "))

            if (largo > 0):

                rellena_lista(lista_a, largo)
                rellena_lista(lista_b, largo)
                rellena_lista(lista_c, largo)

                print("Lista a: ", lista_a)
                print("Lista b: ", lista_b)
                print("Lista c: ", lista_c)

                union_listas(lista_a, lista_b, lista_c, largo)
                break

            else:
                print("Debe ingresar un número entero positivo")

        except:
            print("Por favor, ingrese un valor válido (Entero positivo)")
