# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-


def crear_matriz(matriz, altura_triangulo):

    # Matriz en la cual se alojará el triángulo.
    for i in range(altura_triangulo):
        matriz.append(['X'] * ((altura_triangulo * 2)))


def triangulo_normal(matriz, altura_triangulo):

    print("\nTriángulo normal: \n")
    for i in range(altura_triangulo):
        for j in range(altura_triangulo * 2):

            # Comprueba las posiciones y, de acuerdo a ellas,
            # determina que símbolo o carácter debe ir.
            if(j == altura_triangulo - i - 1):
                matriz[i][j] = '/'
            elif(j == altura_triangulo * 2 - altura_triangulo + i):
                matriz[i][j] = '\\'
            else:
                matriz[i][j] = ' '

            print(matriz[i][j], end='')

        print('')
    print('--' * altura_triangulo)


def triangulo_invertido(matriz, altura_triangulo):

    print("\nTriángulo invertido: \n")
    print('__' * altura_triangulo)
    for i in range(altura_triangulo):
        for j in range(altura_triangulo * 2):
            # Comprueba las posiciones y, de acuerdo a ellas,
            # determina que símbolo o carácter debe ir.
            if(j == i):
                matriz[i][j] = '\\'
            elif(j == altura_triangulo * 2 - i - 1):
                matriz[i][j] = '/'
            else:
                matriz[i][j] = ' '
            print(matriz[i][j], end='')
        print('')


if __name__ == '__main__':

    matriz = []
    
    # Pide si o sí que se ingrese un número entero positivo; de lo
    # contrario, el programa no iniciará.
    while True:
        try:
            altura_triangulo = int(input("Ingrese la altura del triángulo: ", ))

            if(altura_triangulo > 0):
                crear_matriz(matriz, altura_triangulo)
                triangulo_normal(matriz, altura_triangulo)
                triangulo_invertido(matriz, altura_triangulo)
                break

            else:
                print("Debe ingresar un número entero positivo")

        except:
            print("Debe ingresar un valor válido (Solo enteros positivos)")
