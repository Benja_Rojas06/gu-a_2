# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-


def lista_original(limite):

    # Ciclo que crea la lista original.
    lista = [n for n in range(1, limite + 1)]

    print("\nSu lista original es: ", lista)


def menores(numero_inflexion):

    # Nueva lista que contiene a los números menores que k; serán todos los
    # números entre 1 y k.
    lista_menores = [n for n in range(1, numero_inflexion)]

    print("\nLos números menores que k son: ", lista_menores)

def mayores(numero_inflexion, limite):

    # Nueva lista que contiene a los números mayores que k; serán todos los
    # números entre k y el total de datos.
    lista_mayores = [n for n in range(numero_inflexion + 1, limite + 1)]

    print("\nLos números mayores que k son: ", lista_mayores)

def numero_k(numero_inflexion):

    # Número punto inflexión.
    lista_k = [numero_inflexion]

    print("\nEl valor de k es: ", lista_k)

def multiplos(numero_inflexion, limite):

    # Nueva lista que contiene a los múltiplos de k; se comprueba si al
    # calcular su "%" este es 0.
    lista_multiplos = [n for n in range(1, limite + 1) if n % numero_inflexion
                      == 0]

    print("\nLos múltiplos de k son: ", lista_multiplos)

if __name__ == '__main__':

    # Pide si o sí que se ingresen dos números enteros positivos; de lo
    # contrario, el programa no iniciará.
    while True:

        try:
            limite = int(input("Ingrese el límite de los números: "))

            # Netamente para saber a partir de cual número hay que sacar
            # los menores, mayores y el mismo número en cuestión.
            numero_inflexion = int(input("Ingrese el número que será el punto"
                                         "de inflexión (k): "))
            if (limite > 0 and numero_inflexion > 0):

                lista_original(limite)
                menores(numero_inflexion)
                mayores(numero_inflexion, limite)
                numero_k(numero_inflexion)
                multiplos(numero_inflexion, limite)
                break

            else:
                print("Ingrese valores enteros positivos para ambos parámetros")

        except:
            print("Ingrese valores válidos para ambos parámetros")
