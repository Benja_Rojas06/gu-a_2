# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-


import random


#def intercambio()


def generador_matriz(ancho):

    for i in range(ancho):
        matriz.append([])
        for j in range(ancho):
            # Se rellena la matriz con números random entre 10
            # y 100; se comienza desde el 10 para no tener problemas
            # de espacios con los números de 1 dígito (Netamente estético).
            numero = random.randrange(10, 100)
            matriz[i].append([numero])
            print(matriz[i][j], end=" ")
        print()


if __name__ == '__main__':

    matriz = []

    # Pide si o sí que se ingrese un número entero positivo; de lo
    # contrario, el programa no iniciará.
    while True:
        try:
            ancho = int(input("Ingrese el ancho deseado: "))

            if(ancho > 0):
                generador_matriz(ancho)
                #intercambio()
                break

            else:
                print("Debe ingresar un número entero positivo")

        except:
            print("Debe ingresar un valor válido (Solo enteros positivos)")
